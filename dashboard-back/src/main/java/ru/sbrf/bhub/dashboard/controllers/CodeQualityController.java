package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.CodeQualityDto;
import ru.sbrf.bhub.dashboard.services.CodeQualityService;

import java.util.List;

@RestController
@RequestMapping("CodeQuality")
@CrossOrigin
public class CodeQualityController {

    private final CodeQualityService codeQualityService;

    public CodeQualityController(CodeQualityService codeQualityService) {
        this.codeQualityService = codeQualityService;
    }
    @GetMapping("/getCodeQuality")
    public List<CodeQualityDto> getCodeQuality() {
        return codeQualityService.list();
    }
}