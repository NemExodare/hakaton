package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.UserDto;
import ru.sbrf.bhub.dashboard.enums.UserEmotion;
import ru.sbrf.bhub.dashboard.model.User;
import ru.sbrf.bhub.dashboard.services.UserService;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    public void addUser(@RequestBody User user) {
        userService.create(user);
    }

    @PostMapping("/emotion")
    public void setEmotion(@RequestParam UUID userId, @RequestParam String emotion){
        UserDto user = userService.findUserById(userId);
        user.setEmotion(UserEmotion.valueOf(emotion));
        userService.save(user);
    }

    @GetMapping("/list")
    public List<User> list() {
        return userService.list();
    }

    @GetMapping("/list/{userId}")
    public UserDto list(@PathVariable UUID userId) {
        return userService.findUserById(userId);
    }
}
