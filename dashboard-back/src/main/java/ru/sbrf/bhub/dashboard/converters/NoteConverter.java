package ru.sbrf.bhub.dashboard.converters;

import org.springframework.stereotype.Component;
import ru.sbrf.bhub.dashboard.dto.NoteDto;
import ru.sbrf.bhub.dashboard.model.Note;

@Component
public class NoteConverter {

    public NoteDto toDto(Note note) {
        if (note == null) {
            return null;
        }
        return NoteDto.builder()
                .userId(note.getUser().getId())
                .text(note.getText())
                .build();
    }
}
