package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;
import ru.sbrf.bhub.dashboard.enums.UserEmotion;
import ru.sbrf.bhub.dashboard.enums.UserFeeling;

import java.util.UUID;

@Data
@Builder
public class UserDto {

    private UUID id;

    private String login;

    private String firstname;

    private String lastname;

    private String middlename;

    private String photoUrl;

    private UserFeeling feeling;

    private UserEmotion emotion;
}
