package ru.sbrf.bhub.dashboard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;

import java.util.UUID;

public interface BlogRepository extends JpaRepository<Blog, UUID>, BlogRepositoryExtended {

    Blog findByLink(String link);
}
