package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.dto.CodeQualityDto;
import ru.sbrf.bhub.dashboard.dto.JiraClassDto;

import java.util.List;

@Service
public class CodeQualityService {
    public List<CodeQualityDto> list() {
        CodeQualityDto cq1 = CodeQualityDto.builder()
                .assignee("Bolgov-AE")
                .period("2021-08")
                .qtyBugsReportedOn(2)
                .percBugsReportedOn(20)
                .qtyBackOnDev(1)
                .qtyIssueSonar(57)
                .build();
        CodeQualityDto cq2 = CodeQualityDto.builder()
                .assignee("Bolgov-AE")
                .period("2021-09")
                .qtyBugsReportedOn(1)
                .percBugsReportedOn(15)
                .qtyBackOnDev(0)
                .qtyIssueSonar(37)
                .build();

        return List.of(cq1, cq2);
    }
}