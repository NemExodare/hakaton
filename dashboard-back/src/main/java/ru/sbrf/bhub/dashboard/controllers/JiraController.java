package ru.sbrf.bhub.dashboard.controllers;

        import org.springframework.web.bind.annotation.*;
        import ru.sbrf.bhub.dashboard.dto.JiraClassDto;
        import ru.sbrf.bhub.dashboard.services.JiraService;

        import java.util.List;

@RestController
@RequestMapping("jira")
@CrossOrigin
public class JiraController {

    private final JiraService jiraService;

    public JiraController(JiraService jiraService) {
        this.jiraService = jiraService;
    }
    @PostMapping("/getJiraIssues")
    public List<JiraClassDto> getJiraIssues() {
        return jiraService.list();
    }
}