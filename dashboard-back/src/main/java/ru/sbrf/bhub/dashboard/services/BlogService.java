package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.controllers.converters.BlogConverter;
import ru.sbrf.bhub.dashboard.dao.BlogRepository;
import ru.sbrf.bhub.dashboard.dao.UserRepository;
import ru.sbrf.bhub.dashboard.dto.BlogDto;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@Service
public class BlogService {

    private final BlogRepository blogRepository;
    private final BlogConverter blogConverter;
    private final UserRepository userRepository;

    public BlogService(BlogRepository blogRepository, BlogConverter blogConverter, UserRepository userRepository) {
        this.blogRepository = blogRepository;
        this.blogConverter = blogConverter;
        this.userRepository = userRepository;
    }

    @Transactional
    public void create(BlogDto dto, UUID userId) {
        Blog blog = blogRepository.findByLink(dto.getLink());
        if (blog == null) {
            blog = Blog.builder()
                    .name(dto.getName())
                    .systemType(dto.getSystemType())
                    .link(dto.getLink())
                    .preview(dto.getPreview())
                    .users(new HashSet<>())
                    .build();
        }
        blog.getUsers().add(userRepository.getById(userId));
        blogRepository.save(blog);
    }

    public List<BlogDto> list(UUID teamId){
        return CommonUtils.transform(blogRepository.list(teamId), blogConverter::toDto);
    }

    public List<BlogDto> list(){
        return CommonUtils.transform(blogRepository.findAll(), blogConverter::toDto);
    }

    public Blog getById(UUID blogId) {
        return blogRepository.findById(blogId).get();
    }
}
