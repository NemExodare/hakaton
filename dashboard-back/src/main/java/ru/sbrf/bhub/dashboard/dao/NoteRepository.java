package ru.sbrf.bhub.dashboard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sbrf.bhub.dashboard.model.Note;
import ru.sbrf.bhub.dashboard.model.User;

import java.util.UUID;

public interface NoteRepository extends JpaRepository<Note, UUID> {

    Note findByUser(User user);

}
