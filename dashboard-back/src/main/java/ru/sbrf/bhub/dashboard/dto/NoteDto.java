package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class NoteDto {

    private String text;

    private UUID userId;
}
