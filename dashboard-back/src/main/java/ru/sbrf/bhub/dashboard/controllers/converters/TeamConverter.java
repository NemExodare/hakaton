package ru.sbrf.bhub.dashboard.controllers.converters;

import org.springframework.stereotype.Component;
import ru.sbrf.bhub.dashboard.dto.TeamDto;
import ru.sbrf.bhub.dashboard.dto.UserDto;
import ru.sbrf.bhub.dashboard.model.Team;
import ru.sbrf.bhub.dashboard.model.User;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamConverter {

    private final UserConverter userConverter;

    public TeamConverter(UserConverter userConverter) {
        this.userConverter = userConverter;
    }

    public TeamDto toDto(Team team) {
        return TeamDto.builder()
                .id(team.getId())
                .name(team.getName())
                .users(team.getUsers()
                        .stream().map(userConverter::toDto)
                        .sorted(Comparator.comparing(UserDto::getLastname))
                        .collect(Collectors.toList()))
                .build();
    }
}
