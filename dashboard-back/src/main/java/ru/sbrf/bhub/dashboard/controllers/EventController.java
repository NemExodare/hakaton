package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.EventDto;
import ru.sbrf.bhub.dashboard.services.EventService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("event")
@CrossOrigin
public class EventController {

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/create")
    public void create(@RequestBody EventDto eventDto) {
        eventService.create(eventDto);
    }

    @PostMapping("/list")
    public List<EventDto> list(@RequestParam UUID teamId, @RequestParam UUID userId) {
        return eventService.list(teamId, userId);
    }

    @PostMapping("/accept")
    public void accept(@RequestParam UUID eventId, @RequestParam UUID userId) {
        eventService.accept(eventId, userId);
    }
}
