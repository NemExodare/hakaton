package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.PullRequestDto;
import ru.sbrf.bhub.dashboard.services.GitService;

import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("git")
@CrossOrigin
public class GitController {

    private final GitService gitService;

    public GitController(GitService gitService) {
        this.gitService = gitService;
    }

    @PostMapping("/getWaitedPullRequest")
    public Set<PullRequestDto> getWaitedPullRequest(@RequestParam UUID userId) {
        return gitService.getWaitedPullRequests(userId);
    }
}
