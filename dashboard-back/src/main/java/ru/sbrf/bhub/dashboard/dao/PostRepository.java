package ru.sbrf.bhub.dashboard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sbrf.bhub.dashboard.model.User;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.model.confluence.Post;

import java.util.List;
import java.util.UUID;


public interface PostRepository extends JpaRepository<Post, UUID>, PostRepositoryExtended {
    List<Post> findPostsByBlog(Blog blog);
    List<Post> findPostsByBlog_Users(User user);
}
