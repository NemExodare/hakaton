package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.converters.NoteConverter;
import ru.sbrf.bhub.dashboard.dao.NoteRepository;
import ru.sbrf.bhub.dashboard.dao.UserRepository;
import ru.sbrf.bhub.dashboard.dto.NoteDto;
import ru.sbrf.bhub.dashboard.model.Note;
import ru.sbrf.bhub.dashboard.model.User;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
public class NoteService {

    private final NoteRepository notesRepository;
    private final UserRepository userRepository;
    private final NoteConverter noteConverter;

    public NoteService(NoteRepository notesRepository, UserRepository userRepository, NoteConverter noteConverter) {
        this.notesRepository = notesRepository;
        this.userRepository = userRepository;
        this.noteConverter = noteConverter;
    }

    @Transactional
    public void update(NoteDto noteDto) {
        User user = userRepository.getById(noteDto.getUserId());
        Note note = notesRepository.findByUser(user);
        if (note == null) {
            note = new Note();
            note.setUser(user);
        }
        note.setText(noteDto.getText());
        notesRepository.save(note);
    }

    public NoteDto getByUser(UUID userId) {
        User user = userRepository.getById(userId);
        return noteConverter.toDto(notesRepository.findByUser(user));
    }

}
