package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.dao.EventRepository;
import ru.sbrf.bhub.dashboard.dao.TeamRepository;
import ru.sbrf.bhub.dashboard.dao.UserRepository;
import ru.sbrf.bhub.dashboard.dto.EventDto;
import ru.sbrf.bhub.dashboard.model.Event;
import ru.sbrf.bhub.dashboard.model.User;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class EventService {

    private final EventRepository eventRepository;
    private final TeamRepository teamRepository;
    private final UserRepository userRepository;


    public EventService(EventRepository eventRepository, TeamRepository teamRepository, UserRepository userRepository) {
        this.eventRepository = eventRepository;
        this.teamRepository = teamRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void create(EventDto dto) {
        Event event = Event.builder()
                .id(dto.getId())
                .text(dto.getText())
                .type(dto.getType())
                .team(teamRepository.getById(dto.getTeam()))
                .build();
        eventRepository.save(event);
    }

    public List<EventDto> list(UUID teamId, UUID userId) {
        return CommonUtils.transform(eventRepository.list(teamId, userId), this::toDto);
    }

    @Transactional
    public void accept(UUID eventId, UUID userId) {
        Event event = eventRepository.getById(eventId);
        User user = userRepository.getById(userId);
        event.getAcceptedUsers().add(user);
        eventRepository.save(event);
    }

    private EventDto toDto(Event event) {
        return EventDto.builder()
                .id(event.getId())
                .text(event.getText())
                .type(event.getType())
                .team(event.getTeam().getId())
                .build();
    }
}
