package ru.sbrf.bhub.dashboard.dao;

import ru.sbrf.bhub.dashboard.model.confluence.Blog;

import java.util.List;
import java.util.UUID;

public interface BlogRepositoryExtended {
    List<Blog> list(UUID teamId);
}
