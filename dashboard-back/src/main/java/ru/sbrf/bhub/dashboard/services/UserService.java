package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.controllers.converters.UserConverter;
import ru.sbrf.bhub.dashboard.dao.UserRepository;
import ru.sbrf.bhub.dashboard.dto.UserDto;
import ru.sbrf.bhub.dashboard.model.User;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserConverter userConverter;

    public UserService(UserRepository userRepository, UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Transactional
    public void create(User user) {
        userRepository.save(user);
    }

    public void save(UserDto dto) {
        userRepository.save(userConverter.toEntity(dto));
    }

    public List<User> list(){
        return userRepository.findAll();
    }

    public UserDto findUserById(UUID userId){
        return userConverter.toDto(userRepository.findUserById(userId));
    }
}
