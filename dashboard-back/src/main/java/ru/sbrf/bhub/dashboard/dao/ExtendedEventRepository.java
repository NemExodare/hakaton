package ru.sbrf.bhub.dashboard.dao;

import ru.sbrf.bhub.dashboard.model.Event;

import java.util.List;
import java.util.UUID;

public interface ExtendedEventRepository {

    List<Event> list(UUID teamId, UUID userId);
}
