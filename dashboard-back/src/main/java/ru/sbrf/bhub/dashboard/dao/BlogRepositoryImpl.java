package ru.sbrf.bhub.dashboard.dao;

import org.springframework.stereotype.Repository;
import ru.sbrf.bhub.dashboard.model.Team_;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.model.confluence.Blog_;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.UUID;

@Repository
public class BlogRepositoryImpl implements BlogRepositoryExtended {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Blog> list(UUID teamId) {
        SimpleQuery<Blog> simpleQuery = new SimpleQuery<>(entityManager, Blog.class);
        simpleQuery.query.where(
                simpleQuery.builder.equal(simpleQuery.root.get(Blog_.USERS).get(Team_.ID), teamId)
        );
        return simpleQuery.getList();
    }

}
