package ru.sbrf.bhub.dashboard.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.sbrf.bhub.dashboard.enums.UserEmotion;
import ru.sbrf.bhub.dashboard.enums.UserFeeling;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class User extends BaseEntity {

    @Column(name = "login", updatable = false, nullable = false)
    private String login;

    private String firstname;

    private String lastname;

    private String middlename;

    private String photoUrl;

    @Enumerated(EnumType.STRING)
    private UserFeeling feeling;

    @Enumerated(EnumType.STRING)
    private UserEmotion emotion;

}
