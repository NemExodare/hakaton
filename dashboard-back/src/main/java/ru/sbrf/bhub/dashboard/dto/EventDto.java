package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;
import ru.sbrf.bhub.dashboard.enums.EventType;

import java.util.UUID;

@Data
@Builder
public class EventDto {

    private UUID id;

    private String text;

    private EventType type;

    private UUID team;
}
