package ru.sbrf.bhub.dashboard.enums;

public enum UserFeeling {
    SLEEPING,   // coffee_s.png
    ENERGETIC,  // thunderstruck_s.png
    BUSY        // support_s.png
}
