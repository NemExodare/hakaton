package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CodeQualityDto {
    private String assignee;
    private String period; //отчетный период
    private int qtyBugsReportedOn; //баги по задаче
    private int percBugsReportedOn; //доля багов от всех задач
    private int qtyBackOnDev; //возврат в доработку
    private int qtyIssueSonar; //issue в sonar
}
