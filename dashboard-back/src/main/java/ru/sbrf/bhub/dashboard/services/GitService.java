package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.dto.PullRequestDto;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Service
public class GitService {

    private Set<PullRequestDto> pullRequestMocks = createMocks();

    private Set<PullRequestDto> createMocks() {
        PullRequestDto outdated = PullRequestDto.builder()
                .name("BHUB-31: added new form for user management")
                .openDays(3)
                .hasUpdates(true)
                .fromUser("Анохин Илья Сергеевич")
                .build();
        PullRequestDto newPullRequest = PullRequestDto.builder()
                .name("BHUB-32: Ingress config created")
                .openDays(1)
                .hasUpdates(false)
                .fromUser("Анохин Илья Сергеевич")
                .build();
        PullRequestDto outdated2 = PullRequestDto.builder()
                .name("BHUB-14: integration with pprb monitoring")
                .openDays(4)
                .hasUpdates(true)
                .fromUser("Болгов Александр")
                .build();

        return Set.of(outdated, newPullRequest, outdated2);

    }


    public Set<PullRequestDto> getWaitedPullRequests(UUID userId) {
        return pullRequestMocks;
    }
}
