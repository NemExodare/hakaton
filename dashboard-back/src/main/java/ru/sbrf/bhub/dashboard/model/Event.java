package ru.sbrf.bhub.dashboard.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.sbrf.bhub.dashboard.enums.EventType;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Event extends BaseEntity {

    private String text;

    private EventType type;

    @ManyToOne
    @JoinColumn(name="team_id", nullable = false)
    private Team team;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "User_Event",
            joinColumns = { @JoinColumn(name = "event_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private Set<User> acceptedUsers;

}
