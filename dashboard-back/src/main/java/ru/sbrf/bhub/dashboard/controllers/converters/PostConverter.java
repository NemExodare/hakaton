package ru.sbrf.bhub.dashboard.controllers.converters;

import org.springframework.stereotype.Component;
import ru.sbrf.bhub.dashboard.dto.PostDto;
import ru.sbrf.bhub.dashboard.model.confluence.Post;

@Component
public class PostConverter {

    private final BlogConverter blogConverter;

    public PostConverter(BlogConverter blogConverter) {
        this.blogConverter = blogConverter;
    }

    public PostDto toDto(Post post){
        return PostDto.builder()
                .id(post.getId())
                .name(post.getName())
                .preview(post.getPreview())
                .blogId(blogConverter.toDto(post.getBlog()))
                .build();
    }

    public Post toEntity(PostDto dto){
        return Post.builder()
                .id(dto.getId())
                .name(dto.getName())
                .preview(dto.getPreview())
                .link(dto.getLink())
                .build();
    }
}
