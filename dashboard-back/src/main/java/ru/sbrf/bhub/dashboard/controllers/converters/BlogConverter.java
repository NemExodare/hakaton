package ru.sbrf.bhub.dashboard.controllers.converters;

import org.springframework.stereotype.Component;
import ru.sbrf.bhub.dashboard.dto.BlogDto;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;

@Component
public class BlogConverter {

    private PostConverter postConverter;

    public BlogDto toDto(Blog blog){
        return BlogDto.builder()
                .id(blog.getId())
                .name(blog.getName())
                .link(blog.getLink())
                .preview(blog.getPreview())
                .systemType(blog.getSystemType())
                .build();
    }

    public Blog toEntity(BlogDto dto){
        return Blog.builder()
                .id(dto.getId())
                .link(dto.getLink())
                .name(dto.getName())
                .preview(dto.getPreview())
                .build();
    }
}
