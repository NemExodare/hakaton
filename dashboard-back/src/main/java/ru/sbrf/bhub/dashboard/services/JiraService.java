package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.dto.JiraClassDto;

import java.util.List;

@Service
public class JiraService {
    public List<JiraClassDto> list() {
        JiraClassDto jira1 = JiraClassDto.builder()
                .projectName("DEVDASH")
                .issueId("1")
                .issueType("Story")
                .assignee("Bolgov-AE")
                .createdDate("2021-09-01")
                .plannedEndDate("2021-10-01")
                .status("In Progress")
                .build();
        JiraClassDto jira2 = JiraClassDto.builder()
                .projectName("DEVDASH")
                .issueId("2")
                .issueType("Task")
                .assignee("Bolgov-AE")
                .createdDate("2021-09-07")
                .plannedEndDate("2021-09-10")
                .status("In Progress")
                .build();
        JiraClassDto jira3 = JiraClassDto.builder()
                .projectName("DEVDASH")
                .issueId("3")
                .issueType("Bug")
                .assignee("Bolgov-AE")
                .createdDate("2021-09-08")
                .plannedEndDate("2021-09-12")
                .status("In Progress")
                .build();

        return List.of(jira1, jira2, jira3);
    }
}
