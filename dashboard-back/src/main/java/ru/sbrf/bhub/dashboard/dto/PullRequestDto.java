package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class PullRequestDto {
    private String name;
    private String fromUser;
    private int openDays;
    private boolean hasUpdates;
}
