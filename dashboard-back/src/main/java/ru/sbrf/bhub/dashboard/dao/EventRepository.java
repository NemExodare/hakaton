package ru.sbrf.bhub.dashboard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sbrf.bhub.dashboard.model.Event;

import java.util.UUID;

public interface EventRepository extends JpaRepository<Event, UUID>, ExtendedEventRepository {
}
