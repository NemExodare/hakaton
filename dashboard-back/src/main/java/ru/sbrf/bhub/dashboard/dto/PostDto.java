package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class PostDto {

    private UUID id;

    private String name;

    private String preview;

    private String link;

    private BlogDto blogId;
}
