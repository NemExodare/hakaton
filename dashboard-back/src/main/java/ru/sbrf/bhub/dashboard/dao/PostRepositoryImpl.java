package ru.sbrf.bhub.dashboard.dao;

import ru.sbrf.bhub.dashboard.enums.EventType;
import ru.sbrf.bhub.dashboard.model.*;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.model.confluence.Blog_;
import ru.sbrf.bhub.dashboard.model.confluence.Post;
import ru.sbrf.bhub.dashboard.model.confluence.Post_;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.SetJoin;
import java.util.List;
import java.util.UUID;

public class PostRepositoryImpl implements PostRepositoryExtended {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Post> list(UUID blogId) {
        SimpleQuery<Post> simpleQuery = new SimpleQuery<>(entityManager, Post.class);
        simpleQuery.query.where(
                simpleQuery.builder.equal(simpleQuery.root.get(Post_.blog), blogId));
        return simpleQuery.getList();
    }

    @Override
    public List<Post> list() {
        SimpleQuery<Post> simpleQuery = new SimpleQuery<>(entityManager, Post.class);
        return simpleQuery.getList();
    }

}
