package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.NoteDto;
import ru.sbrf.bhub.dashboard.services.NoteService;

import java.util.UUID;

@RestController
@RequestMapping("note")
@CrossOrigin
public class NoteController {

    private final NoteService notesService;

    public NoteController(NoteService notesService) {
        this.notesService = notesService;
    }

    @PostMapping("/update")
    public void update(@RequestBody NoteDto noteDto) {
        notesService.update(noteDto);
    }

    @PostMapping("/get")
    public NoteDto get(@RequestParam UUID userId) {
        return notesService.getByUser(userId);
    }


}
