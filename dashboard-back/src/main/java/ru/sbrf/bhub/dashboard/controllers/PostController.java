package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.PostDto;
import ru.sbrf.bhub.dashboard.model.confluence.Post;
import ru.sbrf.bhub.dashboard.services.PostService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("post")
@CrossOrigin
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/create")
    public void create(@RequestBody PostDto dto) {
        postService.create(dto);
    }

    @GetMapping("/list")
    public List<PostDto> list() {
        return postService.list();
    }

    @GetMapping("/{postId}")
    public Post getById(@PathVariable UUID postId) {
        return postService.getById(postId);
    }

    @GetMapping("/list/filter/blog")
    public List<PostDto> filterByBlog(@RequestParam(required = false, name="blog") UUID blogId) {
        return postService.filterByBlog(blogId);
    }

    @GetMapping("/list/filter/user")
    public List<PostDto> filterByUser(@RequestParam(required = false, name="user")  UUID userId) {
        return postService.filterByUser(userId);
    }

}
