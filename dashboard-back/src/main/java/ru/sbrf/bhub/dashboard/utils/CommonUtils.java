package ru.sbrf.bhub.dashboard.utils;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CommonUtils {

    public static <T, U> List<U> transform(Iterable<T> list, Function<T, U> function) {
        return stream(list).map(function).collect(Collectors.toList());
    }

    public static <T, U> Set<U> transformToSet(Iterable<T> list, Function<T, U> function) {
        return stream(list).map(function).collect(Collectors.toSet());
    }

    public static <T> Stream<T> stream(Iterable<T> iterable) {
        return iterable == null ? Stream.empty() : StreamSupport.stream(iterable.spliterator(), false);
    }
}
