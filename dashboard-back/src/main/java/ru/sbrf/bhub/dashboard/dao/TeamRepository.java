package ru.sbrf.bhub.dashboard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sbrf.bhub.dashboard.model.Team;

import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
}
