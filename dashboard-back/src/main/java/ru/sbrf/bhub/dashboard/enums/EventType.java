package ru.sbrf.bhub.dashboard.enums;

public enum EventType {
    INFORMATION, TASK
}
