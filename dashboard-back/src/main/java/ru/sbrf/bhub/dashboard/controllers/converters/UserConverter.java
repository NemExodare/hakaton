package ru.sbrf.bhub.dashboard.controllers.converters;

import org.springframework.stereotype.Component;
import ru.sbrf.bhub.dashboard.dto.BlogDto;
import ru.sbrf.bhub.dashboard.dto.UserDto;
import ru.sbrf.bhub.dashboard.enums.UserEmotion;
import ru.sbrf.bhub.dashboard.model.User;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

@Component
public class UserConverter {

    public UserDto toDto(User user){
        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .middlename(user.getMiddlename())
                .photoUrl(user.getPhotoUrl())
                .feeling(user.getFeeling())
                .emotion(UserEmotion.valueOf(user.getEmotion().name()))
                .build();
    }

    public User toEntity(UserDto dto) {
        return User.builder()
                .id(dto.getId())
                .emotion(dto.getEmotion())
                .feeling(dto.getFeeling())
                .firstname(dto.getFirstname())
                .lastname(dto.getLastname())
                .middlename(dto.getMiddlename())
                .login(dto.getLogin())
                .photoUrl(dto.getPhotoUrl())
                .build();
    }
}
