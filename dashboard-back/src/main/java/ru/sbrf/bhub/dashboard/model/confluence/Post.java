package ru.sbrf.bhub.dashboard.model.confluence;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.sbrf.bhub.dashboard.model.BaseEntity;
import ru.sbrf.bhub.dashboard.model.Event;
import ru.sbrf.bhub.dashboard.model.Team;
import ru.sbrf.bhub.dashboard.model.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;


@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Post extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "preview", nullable = false)
    private String preview;

    @Column(name = "link", nullable = false)
    private String link;

    @ManyToOne()
    @JoinColumn(name = "blog_id")
    private Blog blog;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime insertDate;

}
