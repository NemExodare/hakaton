package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.controllers.converters.BlogConverter;
import ru.sbrf.bhub.dashboard.controllers.converters.PostConverter;
import ru.sbrf.bhub.dashboard.dao.BlogRepository;
import ru.sbrf.bhub.dashboard.dao.PostRepository;
import ru.sbrf.bhub.dashboard.dao.UserRepository;
import ru.sbrf.bhub.dashboard.dto.PostDto;
import ru.sbrf.bhub.dashboard.model.confluence.Post;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostService {

    private final PostRepository postRepository;
    private final BlogRepository blogRepository;
    private final BlogConverter blogConverter;
    private final UserRepository userRepository;
    private final PostConverter postConverter;

    public PostService(PostRepository postRepository, BlogRepository blogRepository, BlogConverter blogConverter, UserRepository userRepository, PostConverter postConverter) {
        this.postRepository = postRepository;
        this.blogRepository = blogRepository;
        this.blogConverter = blogConverter;
        this.userRepository = userRepository;
        this.postConverter = postConverter;
    }

    public void create(PostDto dto){
        Post post = Post.builder()
                .name(dto.getName())
                .preview(dto.getPreview())
                .blog(blogConverter.toEntity(dto.getBlogId()))
                .link(dto.getLink())
                .build();

        postRepository.save(post);
    }

    public List<PostDto> list(){
        return CommonUtils.transform(postRepository.list(), postConverter::toDto);
    }

    public List<PostDto> filterByBlog(UUID blogId){
        return CommonUtils.transform(postRepository.findPostsByBlog(blogRepository.getById(blogId)), postConverter::toDto);
    }

    public List<PostDto> filterByUser(UUID userId){
        return postRepository.findPostsByBlog_Users(userRepository.getById(userId)).stream()
                .sorted(Comparator.comparing(Post::getInsertDate).reversed())
                .map(postConverter::toDto)
                .collect(Collectors.toList());
    }

    public Post getById(UUID postId) {
        return postRepository.findById(postId).get();
    }
}
