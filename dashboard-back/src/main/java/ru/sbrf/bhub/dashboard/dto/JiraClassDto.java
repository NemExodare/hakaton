package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JiraClassDto {
    private String projectName;
    private String issueId;
    private String issueType;
    private String status;
    private String assignee;
    private String createdDate;
    private String plannedEndDate;
}
