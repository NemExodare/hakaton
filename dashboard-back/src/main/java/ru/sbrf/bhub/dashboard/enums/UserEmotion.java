package ru.sbrf.bhub.dashboard.enums;

public enum UserEmotion {
    ANGEL,
    ANGRY,
    BORED,
    COOL,
    PARTY,
    SLEEPY,
    SURPRISED
}
