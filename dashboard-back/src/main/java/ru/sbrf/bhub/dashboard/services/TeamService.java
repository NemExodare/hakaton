package ru.sbrf.bhub.dashboard.services;

import org.springframework.stereotype.Service;
import ru.sbrf.bhub.dashboard.dao.TeamRepository;
import ru.sbrf.bhub.dashboard.dao.UserRepository;
import ru.sbrf.bhub.dashboard.dto.TeamDto;
import ru.sbrf.bhub.dashboard.model.Team;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class TeamService {

    private final TeamRepository teamRepository;
    private final UserRepository userRepository;

    public TeamService(TeamRepository teamRepository, UserRepository userRepository) {
        this.teamRepository = teamRepository;
        this.userRepository = userRepository;
    }

    public void create(TeamDto dto) {
        Team team = Team.builder()
                .id(dto.getId())
                .name(dto.getName())
                .users(CommonUtils.transformToSet(dto.getUsers(), user -> userRepository.getById(user.getId())))
                .build();

        teamRepository.save(team);
    }

    public List<Team> list(){
        return teamRepository.findAll();
    }

    public Team get(UUID id) {
        return teamRepository.getById(id);
    }

}
