package ru.sbrf.bhub.dashboard.dao;


import ru.sbrf.bhub.dashboard.model.*;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.SetJoin;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class EventRepositoryImpl implements ExtendedEventRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Event> list(UUID teamId, UUID userId) {
        SimpleQuery<Event> simpleQuery = new SimpleQuery<>(entityManager, Event.class);
        SetJoin<Event, User> users = simpleQuery.root.join(Event_.acceptedUsers, JoinType.LEFT);
        simpleQuery.query.where(simpleQuery.builder.equal(simpleQuery.root.get(Event_.team).get(Team_.id), teamId));
        return simpleQuery.getList().stream()
                .filter(event -> event.getAcceptedUsers().isEmpty() || !CommonUtils.transform(event.getAcceptedUsers(), User::getId)
                        .contains(userId)).collect(Collectors.toList());
    }
}
