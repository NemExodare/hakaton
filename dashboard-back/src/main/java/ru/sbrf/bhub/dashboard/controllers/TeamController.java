package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.controllers.converters.TeamConverter;
import ru.sbrf.bhub.dashboard.dto.TeamDto;
import ru.sbrf.bhub.dashboard.services.TeamService;
import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("team")
public class TeamController {

    private final TeamService teamService;
    private final TeamConverter teamConverter;

    public TeamController(TeamService teamService, TeamConverter teamConverter) {
        this.teamService = teamService;
        this.teamConverter = teamConverter;
    }

    @PostMapping("/create")
    public void addUser(@RequestBody TeamDto teamDto) {
        teamService.create(teamDto);
    }

    @GetMapping("/list")
    public List<TeamDto> list() {
        return CommonUtils.transform(teamService.list(), teamConverter::toDto);
    }

    @GetMapping("/get")
    public TeamDto get(@RequestParam UUID id) {
        return teamConverter.toDto(teamService.get(id));
    }
}
