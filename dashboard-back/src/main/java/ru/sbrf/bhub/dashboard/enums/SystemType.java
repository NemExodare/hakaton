package ru.sbrf.bhub.dashboard.enums;

public enum SystemType {
    CONFLUENCE,
    TELEGRAM,
    SBERCHAT
}
