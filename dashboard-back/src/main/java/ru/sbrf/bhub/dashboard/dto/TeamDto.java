package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
public class TeamDto {

    private UUID id;
    private String name;
    private List<UserDto> users;

}
