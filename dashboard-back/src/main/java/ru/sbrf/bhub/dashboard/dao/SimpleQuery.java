package ru.sbrf.bhub.dashboard.dao;

import ru.sbrf.bhub.dashboard.utils.CommonUtils;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;

public class SimpleQuery<T> {

    private EntityManager entityManager;
    private Class<T> entityClass;
    public CriteriaBuilder builder;
    public CriteriaQuery<T> query;
    public Root<T> root;
    public CriteriaQuery<T> select;

    public SimpleQuery(EntityManager entityManager, Class<T> entityClass) {
        this.entityManager = entityManager;
        this.entityClass = entityClass;
        builder = entityManager.getCriteriaBuilder();
        query = builder.createQuery(entityClass);
        root = query.from(entityClass);
        select = query.select(root);
    }


    protected List<T> getList() {
        return entityManager.createQuery(select).getResultList();
    }


//    protected T getFirst() {
//        return CommonUtils.first(entityManager.createQuery(select).setMaxResults(1).getResultList());
//    }
//

//    protected boolean exists() {
//        return !entityManager.createQuery(query.select((Expression) builder.literal(Boolean.TRUE))).setMaxResults(1).getResultList().isEmpty();
//    }

}
