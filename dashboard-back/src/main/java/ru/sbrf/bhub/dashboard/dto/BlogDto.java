package ru.sbrf.bhub.dashboard.dto;

import lombok.Builder;
import lombok.Data;
import ru.sbrf.bhub.dashboard.enums.SystemType;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class BlogDto {

    private UUID id;

    private SystemType systemType;

    private String link;

    private String name;

    private String preview;

}
