package ru.sbrf.bhub.dashboard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sbrf.bhub.dashboard.model.User;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    User findUserById(UUID userId);
}
