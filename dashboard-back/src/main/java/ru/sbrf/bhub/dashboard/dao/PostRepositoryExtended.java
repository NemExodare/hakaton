package ru.sbrf.bhub.dashboard.dao;


import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.model.confluence.Post;

import java.util.List;
import java.util.UUID;

public interface PostRepositoryExtended {
    List<Post> list(UUID blogId);
    List<Post> list();
}
