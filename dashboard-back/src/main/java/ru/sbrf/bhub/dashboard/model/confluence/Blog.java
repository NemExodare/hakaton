package ru.sbrf.bhub.dashboard.model.confluence;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
//import ru.sbrf.bhub.dashboard.enums.SystemType;
import ru.sbrf.bhub.dashboard.enums.SystemType;
import ru.sbrf.bhub.dashboard.model.BaseEntity;
import ru.sbrf.bhub.dashboard.model.Team;
import ru.sbrf.bhub.dashboard.model.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class Blog extends BaseEntity {

    private SystemType systemType;

    @Column(name="link", nullable = false)
    private String link;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "Blog_User",
            joinColumns = { @JoinColumn(name = "blog_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private Set<User> users = new HashSet<>();

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "preview", nullable = false)
    private String preview;

//    @ManyToMany(cascade = { CascadeType.ALL })
//    @JoinTable(
//            name = "User_Blog",
//            joinColumns = { @JoinColumn(name = "user_id") },
//            inverseJoinColumns = { @JoinColumn(name = "blog_id") }
//    )
//    private Set<User> subscribedUsers = new HashSet<>();
}
