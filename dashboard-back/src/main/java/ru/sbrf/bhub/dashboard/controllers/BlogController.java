package ru.sbrf.bhub.dashboard.controllers;

import org.springframework.web.bind.annotation.*;
import ru.sbrf.bhub.dashboard.dto.BlogDto;
import ru.sbrf.bhub.dashboard.model.User;
import ru.sbrf.bhub.dashboard.model.confluence.Blog;
import ru.sbrf.bhub.dashboard.model.confluence.Post;
import ru.sbrf.bhub.dashboard.services.BlogService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("blog")
@CrossOrigin
public class BlogController {

    private final BlogService blogService;

    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }

    @GetMapping("/{blogId}")
    public Blog getById(@PathVariable UUID blogId) {
        return blogService.getById(blogId);
    }

    @PostMapping("/create")
    public void create(@RequestBody BlogDto dto, @RequestParam UUID userId) {
        blogService.create(dto, userId);
    }

    @GetMapping("/list")
    public List<BlogDto> list() {
        return blogService.list();
    }

    @GetMapping("/list/filter/{teamId}")
    public List<BlogDto> list(@PathVariable UUID teamId) {
        return blogService.list(teamId);
    }


}
