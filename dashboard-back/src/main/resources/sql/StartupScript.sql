insert into user (firstname, lastname, login, middlename, photo_url, id, emotion) values ('Илья', 'Анохин', 'ilya', 'Андреевич', '', '3a25da55-20ab-4d3f-b777-2e1254b4dd5e', 'COOL');
insert into user (firstname, lastname, login, middlename, photo_url, id, emotion) values ('Александра', 'Кононова', 'alek', 'Ивановна', '', 'a3adb37e-aa2d-4e38-af35-d3bc2b7ce417', 'ANGEL');
insert into user (firstname, lastname, login, middlename, photo_url, id, emotion) values ('Александр', 'Болгов', 'bolgov', 'Андреевич', '', '767f6d9a-6156-433f-9d4c-c8b25ec195c0', 'SLEEPY');
insert into user (firstname, lastname, login, middlename, photo_url, id, emotion) values ('Глушков', 'Ярослав', 'yaroslav', 'Андреевич', '', '5c9d7d9a-abdb-48b1-a54f-592597cd71b6', 'SURPRISED');
insert into user (firstname, lastname, login, middlename, photo_url, id, emotion) values ('Мотозюк', 'Владислав', 'vlad', 'Андреевич', '', 'abc06fef-fb52-4d60-8939-09ffe2466510', 'PARTY');

insert into team (name, id) values ('Управление каналом', 'baa62149-1638-444d-bccb-636aa4c0b9d7');
insert into team_user (team_id, user_id) values ('baa62149-1638-444d-bccb-636aa4c0b9d7', '3a25da55-20ab-4d3f-b777-2e1254b4dd5e');
insert into team_user (team_id, user_id) values ('baa62149-1638-444d-bccb-636aa4c0b9d7', 'a3adb37e-aa2d-4e38-af35-d3bc2b7ce417');
insert into team_user (team_id, user_id) values ('baa62149-1638-444d-bccb-636aa4c0b9d7', '767f6d9a-6156-433f-9d4c-c8b25ec195c0');
insert into team_user (team_id, user_id) values ('baa62149-1638-444d-bccb-636aa4c0b9d7', '5c9d7d9a-abdb-48b1-a54f-592597cd71b6');
insert into team_user (team_id, user_id) values ('baa62149-1638-444d-bccb-636aa4c0b9d7', 'abc06fef-fb52-4d60-8939-09ffe2466510');

insert into event (id, text, type, team_id) values ('949abea5-7ae9-4703-802a-3da7e2177048', 'Отправить самооценку в Пульсе', '0', 'baa62149-1638-444d-bccb-636aa4c0b9d7');
insert into event (id, text, type, team_id) values ('f4648c23-a37f-4abe-8c1f-7ae0ee0e8f01', 'Перевыпустить и заменить SSL-сертификаты', '0', 'baa62149-1638-444d-bccb-636aa4c0b9d7');
insert into event (id, text, type, team_id) values ('dd2b2e68-7b8b-4349-a2a2-7d05abe1aa16', 'Необходимо регулярно обновлять документацию в Confluence', '0', 'baa62149-1638-444d-bccb-636aa4c0b9d7');
insert into event (id, text, type, team_id) values ('fe3f41ff-e056-4a77-a4b4-39c50d55b22a', 'До 07.10 подтвердить участие в хакатоне', '0', 'baa62149-1638-444d-bccb-636aa4c0b9d7');

insert into blog (id, link, name, preview, system_type) values ('6036276b-1566-4c19-aa13-eb5154e9407b', 'pageId=1', 'Openshift', 'Введение в Openshift и Kubernetes', '0');
insert into blog (id, link, name, preview, system_type) values ('78688817-14fd-4e3a-9e26-0702b4c58555', 'pageId=2', 'Работа с Git', 'Всё об управлении версиями исходного кода', '2');
insert into blog (id, link, name, preview, system_type) values ('e2cbc33e-994a-477c-a16b-20050410d2b2', 'pageId=3', 'Каталог шаблонов SberWorks', 'Каталог полезных ссылок на заявки', '1');
insert into blog (id, link, name, preview, system_type) values ('64714911-8fe1-43c3-852d-10f8b6aeb966', 'pageId=4', 'Новости RLM', 'Меньше ЗНО с каждым релизом', '1');
insert into blog (id, link, name, preview, system_type) values ('52d5ad97-35e4-49fa-b235-0fec20e02d98', 'pageId=5', 'ФП Ролевые Модели', 'ФП Ролевые Модели', '2');
insert into blog (id, link, name, preview, system_type) values ('d674fca8-f4bd-4a36-8044-bcd2461e4ec7', 'pageId=6', 'M-APP', 'Всё о процессе разработки продуктов в Сбере', '0');

insert into post (id, link, name, preview, blog_id) values ('0e80d1e0-f103-45c2-96c6-950329cf39c4', 'pageId=11', 'Ingress-Egress', 'Начало работы с Istio Ingress/Egress', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into post (id, link, name, preview, blog_id) values ('8a7f18a3-881a-464e-8796-5f5630d97611', 'pageId=12', 'Synapse Control Plane', 'Централизованное управление конфигурацией сайдкаров', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into post (id, link, name, preview, blog_id) values ('b64458c1-e0cd-4872-b88d-465d35586c0e', 'pageId=13', 'Istio', 'Коротко о технологии Service Mesh', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into post (id, link, name, preview, blog_id) values ('2c4359c9-723e-459e-b21d-24b2a6b79e73', 'pageId=21', 'Что такое Git', 'Справочник базовых команд', '78688817-14fd-4e3a-9e26-0702b4c58555');
insert into post (id, link, name, preview, blog_id) values ('c6e29197-2d68-4617-aeb8-1dd3a77988c1', 'pageId=22', 'GitFlow', 'Сила или могила?', '78688817-14fd-4e3a-9e26-0702b4c58555');
insert into post (id, link, name, preview, blog_id) values ('043d0448-3897-4150-a17a-a0e6ed0d74bc', 'pageId=23', 'Как откатить коммит', 'И ещё несколько способов избежать merge-hell', '78688817-14fd-4e3a-9e26-0702b4c58555');
insert into post (id, link, name, preview, blog_id) values ('3de0e58d-bd13-4ca0-ac6f-7fd04e243cb6', 'pageId=31', 'Каталог шаблонов', 'На все случаи жизни', 'e2cbc33e-994a-477c-a16b-20050410d2b2');
insert into post (id, link, name, preview, blog_id) values ('1bf72f40-6873-443a-adb1-92c75fcab1b5', 'pageId=32', 'Правила оформления тикетов', 'Каждая ошибка увеличивает время ожидания', 'e2cbc33e-994a-477c-a16b-20050410d2b2');
insert into post (id, link, name, preview, blog_id) values ('1000df60-a386-4378-8649-ff21f4b986e1', 'pageId=33', 'Нестандартные тикеты', 'Не закрываются никогда', 'e2cbc33e-994a-477c-a16b-20050410d2b2');
insert into post (id, link, name, preview, blog_id) values ('dc89cfe5-f128-45c0-a0b1-4f25cb1860d7', 'pageId=41', 'Новости обновлений', 'Новые сервисы и не только', '64714911-8fe1-43c3-852d-10f8b6aeb966');
insert into post (id, link, name, preview, blog_id) values ('2ef75970-8176-415a-8478-3f5e68a8141c', 'pageId=42', 'Выпуск SSL-сертификатов', 'Удобный сервис для выпуска сертификатов', '64714911-8fe1-43c3-852d-10f8b6aeb966');
insert into post (id, link, name, preview, blog_id) values ('9a820c20-5f3b-4e12-92e2-9f45822763f2', 'pageId=43', 'Кросс-доменная синхронизация Bitbucket', 'Невозможное возможно', '64714911-8fe1-43c3-852d-10f8b6aeb966');
insert into post (id, link, name, preview, blog_id) values ('ade5df4a-e306-4092-8e1a-913a407c87e8', 'pageId=51', 'Атрибутный состав таблиц', 'Инструкция для создания собственных Dto', '52d5ad97-35e4-49fa-b235-0fec20e02d98');
insert into post (id, link, name, preview, blog_id) values ('c969276f-d19a-4636-9b33-2c92697869ac', 'pageId=52', 'Работа с API', 'Описание сущностей и методов', '52d5ad97-35e4-49fa-b235-0fec20e02d98');
insert into post (id, link, name, preview, blog_id) values ('1316513a-ab90-4b2d-a31e-2cd3f45c51ff', 'pageId=53', 'Контакты', 'Каталог полежных ссылок и контактов', '52d5ad97-35e4-49fa-b235-0fec20e02d98');
insert into post (id, link, name, preview, blog_id) values ('11e81ec6-a1af-4282-ae11-c57ada4e9a83', 'pageId=61', 'Новости обновлений', 'Новые функции и сервисы', 'd674fca8-f4bd-4a36-8044-bcd2461e4ec7');
insert into post (id, link, name, preview, blog_id) values ('0e70bad8-8cab-4fea-b0b0-e8e38260ef5d', 'pageId=62', 'Отмена шлагбаумов', 'Долой шлагбаумы, даёшь agile!', 'd674fca8-f4bd-4a36-8044-bcd2461e4ec7');
insert into post (id, link, name, preview, blog_id) values ('82be110b-0dbb-4d2e-8921-64a7ea52487e', 'pageId=63', 'Отмена отмены шлагбаумов', 'Допрыгались, дружочки', 'd674fca8-f4bd-4a36-8044-bcd2461e4ec7');

insert into blog_user (user_id, blog_id) values ('3a25da55-20ab-4d3f-b777-2e1254b4dd5e', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into blog_user (user_id, blog_id) values ('3a25da55-20ab-4d3f-b777-2e1254b4dd5e', '78688817-14fd-4e3a-9e26-0702b4c58555');
insert into blog_user (user_id, blog_id) values ('3a25da55-20ab-4d3f-b777-2e1254b4dd5e', 'e2cbc33e-994a-477c-a16b-20050410d2b2');
insert into blog_user (user_id, blog_id) values ('a3adb37e-aa2d-4e38-af35-d3bc2b7ce417', '64714911-8fe1-43c3-852d-10f8b6aeb966');
insert into blog_user (user_id, blog_id) values ('a3adb37e-aa2d-4e38-af35-d3bc2b7ce417', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into blog_user (user_id, blog_id) values ('a3adb37e-aa2d-4e38-af35-d3bc2b7ce417', '52d5ad97-35e4-49fa-b235-0fec20e02d98');
insert into blog_user (user_id, blog_id) values ('767f6d9a-6156-433f-9d4c-c8b25ec195c0', 'd674fca8-f4bd-4a36-8044-bcd2461e4ec7');
insert into blog_user (user_id, blog_id) values ('767f6d9a-6156-433f-9d4c-c8b25ec195c0', '64714911-8fe1-43c3-852d-10f8b6aeb966');
insert into blog_user (user_id, blog_id) values ('767f6d9a-6156-433f-9d4c-c8b25ec195c0', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into blog_user (user_id, blog_id) values ('5c9d7d9a-abdb-48b1-a54f-592597cd71b6', '78688817-14fd-4e3a-9e26-0702b4c58555');
insert into blog_user (user_id, blog_id) values ('5c9d7d9a-abdb-48b1-a54f-592597cd71b6', 'e2cbc33e-994a-477c-a16b-20050410d2b2');
insert into blog_user (user_id, blog_id) values ('5c9d7d9a-abdb-48b1-a54f-592597cd71b6', '52d5ad97-35e4-49fa-b235-0fec20e02d98');
insert into blog_user (user_id, blog_id) values ('abc06fef-fb52-4d60-8939-09ffe2466510', '6036276b-1566-4c19-aa13-eb5154e9407b');
insert into blog_user (user_id, blog_id) values ('abc06fef-fb52-4d60-8939-09ffe2466510', '64714911-8fe1-43c3-852d-10f8b6aeb966');
insert into blog_user (user_id, blog_id) values ('abc06fef-fb52-4d60-8939-09ffe2466510', '78688817-14fd-4e3a-9e26-0702b4c58555');