import * as React from 'react';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Team from './components/Team';
import IFTStatus from './components/IFTStatus';
import DEVStatus from './components/DEVStatus';
import Header from './components/Header';
import Card from '@mui/material/Card';
import Note from './components/Notes/Note';
import AlertList from './components/AlertList/AlertList';
import JiraTask from './components/JiraTask';
import CodeKPI from './components/CodeKPI';
import { styled } from '@mui/material/styles';
import PullRequests from "./components/Git/PullRequests";
import { useEffect, useState } from "react";
import axios from "axios";
import party from "./assets/feelings/party.png";
import angel from "./assets/feelings/angel.png";
import bored from "./assets/feelings/bored.png";
import cool from "./assets/feelings/cool.png";
import sleepy from "./assets/feelings/sleepy.png";
import surprised from "./assets/feelings/surprised.png";
import Blogs from "./components/Blog/Blogs";
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';

const ITEM_HEIGHT = 250;
const ITEM_PADDING_TOP = 0;


function App() {

  const widgets = [
    {
      id: 1,
      name:'Команда'
    },
    {
      id: 2,
      name:'Мои задачи Jira'
    },
    {
      id: 3,
      name:'Уведомления'
    },
    {
      id: 4,
      name:'Заметки'
    },
    {
      id: 5,
      name:'IFT_Graffana'
    },
    {
      id: 6,
      name:'DEV_Graffana'
    },
    {
      id: 7,
      name:'KPI'
    },
    {
      id: 8,
      name:'Пулл реквесты'
    },
    {
      id: 9,
      name:'Блог'
    },
  ];

  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    
    
    value.forEach(function(item, i, value) {
      if(value.indexOf( 'Команда' ) != -1){
        setTeamWidget(true);
      }
      else{
        setTeamWidget(false);
      }
      if(value.indexOf( 'Мои задачи Jira' ) != -1){
        setTaskWidget(true);
      }
      else{
        setTaskWidget(false);
      }
      if(value.indexOf( 'Уведомления' ) != -1){
        setNotifyWidget(true);
      }
      else{
        setNotifyWidget(false);
      }
      if(value.indexOf( 'Заметки' ) != -1){
        setNoteWidget(true);
      }
      else{
        setNoteWidget(false);
      }
      if(value.indexOf( 'IFT_Graffana' ) != -1){
        setIFTWidget(true);
      }
      else{
        setIFTWidget(false);
      }
      if(value.indexOf( 'DEV_Graffana' ) != -1){
        setDEVWidget(true);
      }
      else{
        setDEVWidget(false);
      }
      if(value.indexOf( 'KPI' ) != -1){
        setKPIWidget(true);
      }
      else{
        setKPIWidget(false);
      }
      if(value.indexOf( 'Пулл реквесты' ) != -1){
        setPullReqWidget(true);
      }
      else{
        setPullReqWidget(false);
      }
      if(value.indexOf( 'Блог' ) != -1){
        setBlogWidget(true);
      }
      else{
        setBlogWidget(false);
      }
    });

    setWidgetName(
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  const emotions = [{ id: "PARTY", img: party }, { id: "ANGEL", img: angel }, { id: "BORED", img: bored }, { id: "COOL", img: cool }, { id: "SLEEPY", img: sleepy }, { id: "SURPRISED", img: surprised }];

  const [team, setTeam] = useState({});
  const [user, setUser] = useState({});
  const [widgetName, setWidgetName] = useState(widgets.map((widget) => (widget.name)));
  const [teamWidget, setTeamWidget] = useState(true);
  const [taskWidget, setTaskWidget] = useState(true);
  const [notifyWidget, setNotifyWidget] = useState(true);
  const [noteWidget, setNoteWidget] = useState(true);
  const [iFTWidget, setIFTWidget] = useState(true);
  const [dEVWidget, setDEVWidget] = useState(true);
  const [kPIWidget, setKPIWidget] = useState(true);
  const [pullReqWidget, setPullReqWidget] = useState(true);
  const [blogWidget, setBlogWidget] = useState(true);

  const teamId = "baa62149-1638-444d-bccb-636aa4c0b9d7"
  const userId = "3a25da55-20ab-4d3f-b777-2e1254b4dd5e"

  const getTeam = async () => {
    try {
      const response = await axios.get(`http://localhost:8080/team/get?id=${teamId}`);
      setTeam(response.data);
    } catch (err) {
      console.error(err);
    }
  };

  const getUser = async () => {
    try {
      const response = await axios.get(`http://localhost:8080/user/list/${userId}`);
      setUser(response.data);
    } catch (err) {
      console.error(err);
    }
  };


  useEffect(() => {
    getTeam();
    getUser();
  }, []);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12} lg={12}>
          <Item>
            <Header team={team} user={user} emotions={emotions} updateTeam={getTeam} />
          </Item>
        </Grid>
        <Grid item xs={12} md={12} lg={12}>
          <FormControl sx={{ m: 1, width: 300, float: 'right' }}>
            <InputLabel id="demo-multiple-checkbox-label">Виджеты</InputLabel>
            <Select
              labelId="demo-multiple-checkbox-label"
              id="demo-multiple-checkbox"
              multiple
              value={widgetName}
              onChange={handleChange}
              input={<OutlinedInput label="Виджеты" />}
              renderValue={(selected) => selected.join(', ')}
              MenuProps={MenuProps}
            >
              {widgets.map((widget) => (
                <MenuItem key={widget.id} value={widget.name}>
                  <Checkbox checked={widgetName.indexOf(widget.name) > -1} />
                  <ListItemText primary={widget.name} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        {teamWidget ? <Grid item lg={3}>
          <Item>
            <Team team={team} emotions={emotions} userId={userId} />
          </Item>
        </Grid> : null}
        {taskWidget ?  <Grid item lg={3}>
          <Item>
            <JiraTask />
          </Item>
        </Grid> : null}
       {notifyWidget ? <Grid item lg={3}>
          <Item>
            <AlertList team={team} user={user} />
          </Item>
        </Grid> : null}
        {noteWidget ? <Grid item lg={3}>
          <Item>
            <Note />
          </Item>
        </Grid> : null}
        {iFTWidget ? <Grid item lg={4}>
          <Item>
            <IFTStatus />
          </Item>
        </Grid>: null }
        {dEVWidget ? <Grid item lg={4}>
          <Item>
            <DEVStatus />
          </Item>
        </Grid> : null}
        {
          kPIWidget ? <Grid item lg={4}>
          <Item>
            <CodeKPI />
          </Item>
        </Grid> : null
        }
        {pullReqWidget ? <Grid item lg={6}>
          <Item>
            <PullRequests userId={user.id} />
          </Item>
        </Grid> : null}
        {
          blogWidget ? <Grid item lg={6}>
          <Item>
            <Blogs userId={user.id} />
          </Item>
        </Grid> : null}
        

      </Grid>
    </Box>
  );
}



const Item = styled(Card)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  boxShadow: '0 4px 24px 0 rgb(34 41 47 / 10%)'
}));

export default App;
