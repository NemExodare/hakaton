import * as React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

function DEVStatus() {
    return (
        <CardContent>
            <iframe
                src="https://snapshot.raintank.io/dashboard-solo/snapshot/dZWeUU2bA1UCOr0xiIieDQNlCAgVkWyG?orgId=2&from=1633654526161&to=1633676126161&theme=light&panelId=2" 
                width="100%" height="100%" frameBorder="0"></iframe>
        </CardContent>
    );
}

export default DEVStatus;