import * as React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import Divider from '@mui/material/Divider';
import { useEffect, useState } from "react";
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import axios from "axios";

function CodeKPI() {
    const [checked, setChecked] = useState(true);
    const [quality, setQuality] = useState({});

    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    const getQuality = async () => {
        try {
            const response = await axios.get(`http://localhost:8080/CodeQuality/getCodeQuality`);
            console.log(response.data);
            setQuality(response.data);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        getQuality();
    }, {});
    return (
        <CardContent sx={{ textAlign: 'center' }}>
            <Stack
                direction="row"
                divider={<Divider orientation="vertical" flexItem />}
                spacing={2}
                justifyContent="space-between"
                alignItems="center"
            >
                <Paper elevation={0}>
                    <Typography gutterBottom variant="h4" component="div">
                        {checked ?  quality[0] && quality[0].percBugsReportedOn : quality[0] && quality[0].qtyBugsReportedOn}{checked ? '%.' : 'шт.'}
                    </Typography>
                    <Typography gutterBottom variant="subtitle1" component="div">
                        Кол-во заведенных bugs
                    </Typography>
                </Paper>
                <Paper elevation={0}>
                    <Typography gutterBottom variant="h4" component="div">
                        {quality[0] && quality[0].qtyBackOnDev}шт.
                    </Typography>
                    <Typography gutterBottom variant="subtitle1" component="div">
                        Кол-во возвратов на доработку
                    </Typography>
                </Paper>
                <Paper elevation={0}>
                    <Typography gutterBottom variant="h4" component="div">
                        {quality[0] && quality[0].qtyIssueSonar}шт.
                    </Typography>
                    <Typography gutterBottom variant="subtitle1" component="div">
                        Кол-во предупреждений в SonarQube
                    </Typography>
                </Paper>
            </Stack>
            <FormControlLabel
                control={
                    <Switch
                        checked={checked}
                        onChange={handleChange}
                        inputProps={{ 'aria-label': 'controlled' }}
                    />}
                label="в процентах"
            />
        </CardContent>
    );
}

export default CodeKPI;