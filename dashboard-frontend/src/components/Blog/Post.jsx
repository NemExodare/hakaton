import React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import pr from "../../assets/pr.png";
import Link from "@mui/material/Link";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import {StyledBadge} from "../Header";
import telegram from "../../assets/telegram.png"
import sberchat from "../../assets/sberchat.png"
import confluence from "../../assets/confluence.png"

function Post(props) {

    const {data} = props;
    const systemType = data.blogId.systemType;
    const systems = [{id: "CONFLUENCE", img: confluence}, {id: "TELEGRAM", img: telegram}, {id: "SBERCHAT", img: sberchat}]

  return (
    <CardContent sx={{ textAlign: 'left', padding: "2px" }}>
        <ListItem>
            <ListItemAvatar>
                <Avatar src={systems.find(system => system.id === systemType).img}/>
            </ListItemAvatar>
            <Link href={data.link}><ListItemText primary={data.name} secondary={`${data.preview}...`} /></Link>
        </ListItem>
        <Divider variant="inset" component="li" />
    </CardContent>
  );
};


export default Post;
