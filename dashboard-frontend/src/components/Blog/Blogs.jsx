import React, {useEffect, useState} from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import List from "@material-ui/core/List";
import axios from "axios";
import Post from "./Post";

function Blogs(props) {
    const [posts, setPosts] = useState([]);

    const getPosts = async () => {
        try {
            const response = await axios.get(`http://localhost:8080/post/list/filter/user?user=${props.userId}`);
            setPosts(response.data);
        } catch (err) {
            console.error(err);
        }
    };


    useEffect(() => {
        if (props.userId) {
            getPosts();
            let intervalId = setInterval(getPosts, 30000);
        }
    }, [props.userId]);

  return (
    <CardContent sx={{ textAlign: 'center' }}>
      <Typography gutterBottom variant="h6" component="div">
        Блог
      </Typography>
      <div style={{height: '300px', overflowY: 'scroll'}}>
      {posts && posts.length > 0 ?  <List>
          {posts.map((post, index) => (
              <Post data={post} key={index.toString()} />
          ))}
      </List> : <Typography sx={{marginTop: '5px'}} gutterBottom variant="body2" component="div">Нет новых записей</Typography>}
      </div>
      
    </CardContent>
  );
};

export default Blogs
