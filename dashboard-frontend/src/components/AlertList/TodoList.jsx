import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import DoneOutline from '@material-ui/icons/DoneOutline';

const TodoList = ({ todos, onDone }) => (
  <List>
    {todos.map((todo, index) => (
      <ListItem key={index} dense button>
        {/*<Checkbox tabIndex={-1} disableRipple />*/}
        <ListItemText primary={todo.text} />
        <ListItemSecondaryAction>
          <IconButton
            aria-label="Done"
            onClick={() => {
              onDone(todo.id);
            }}
          >
            <DoneOutline />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    ))}
  </List>
);

export default TodoList;
