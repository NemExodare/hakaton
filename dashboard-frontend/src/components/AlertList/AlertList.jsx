import React, {useEffect, useState} from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import TodoList from './TodoList';
import useTodoState from './useTodoState';
import axios from "axios";

function AlertList(props) {

  const [ events, setEvents ] = useState([]);

    const fetchEvents = async () => {
        try {
            const response = await axios.post(`http://localhost:8080/event/list?teamId=${props.team.id}&userId=${props.user.id}`);
            console.log('EVENTS', response.data);
            setEvents(response.data);
        } catch (err) {
            console.error(err);
        }
    }

    const completeTodo = async (eventId) => {
        try {
            const response = await axios.post(`http://localhost:8080/event/accept?eventId=${eventId}&userId=${props.user.id}`);
            fetchEvents();
        } catch (err) {
            console.error(err);
        }
    }


    useEffect(() => {
        console.log("useEffect")
        if (props.team.id && props.user.id) {
            fetchEvents();
            let intervalId = setInterval(fetchEvents, 30000);
        }
    }, [props.team, props.user]);



    return (
    <CardContent sx={{ textAlign: 'center' }}>
      <Typography gutterBottom variant="h6" component="div">
        Уведомления
      </Typography>
      {events && events.length > 0 ? <TodoList todos={events} onDone={completeTodo}/> : <Typography sx={{marginTop: '5px'}} gutterBottom variant="body2" component="div">Нет уведомлений</Typography>}
    </CardContent>
  );
};

export default AlertList;
