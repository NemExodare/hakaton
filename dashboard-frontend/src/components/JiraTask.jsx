import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import task from '../assets/task.svg';
import WarningAmberIcon from '@mui/icons-material/WarningAmber';

export default function InsetDividers() {
  return (
    <CardContent>
        <Typography gutterBottom variant="h6" component="div">
                Мои задачи Jira
            </Typography>
    <List
      sx={{
        width: '100%',
        bgcolor: 'background.paper',
      }}
    >
      <ListItem>
        <ListItemAvatar>
        <Avatar alt="Remy Sharp" src={task}/>
        </ListItemAvatar>
        <Link href="#"><ListItemText primary="Придумать концепт UI" secondary="09.10.2021" /></Link>
        <WarningAmberIcon sx={{color: 'red', marginLeft: '5px'}}/>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
        <Avatar alt="Remy Sharp" src={task}/>
        </ListItemAvatar>
        <Link href="#"><ListItemText primary="Разработать дизайн" secondary="14.10.2021" /></Link>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src={task}/>
        </ListItemAvatar>
        <Link href="#"><ListItemText primary="Разработать Web приложение" secondary="21.10.2021" /></Link>
      </ListItem>
    </List>
    </CardContent>
  );
}
