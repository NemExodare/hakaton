import React, {useEffect, useState} from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
// import useTodoState from './useTodoState';
import List from "@material-ui/core/List";
import PullRequest from "./PullRequest";
import axios from "axios";

function PullRequests(props) {
    const [pullRequests, setPullRequests] = useState([]);

    const getPullRequests = async () => {
        try {
            const response = await axios.post(`http://localhost:8080/git/getWaitedPullRequest?userId=${props.userId}`);
            setPullRequests(response.data);
        } catch (err) {
            console.error(err);
        }
    };


    useEffect(() => {
        if (props.userId) {
            getPullRequests();
            let intervalId = setInterval(getPullRequests, 30000);
        }
    }, [props.userId]);

  return (
    <CardContent sx={{ textAlign: 'center' }}>
      <Typography gutterBottom variant="h6" component="div">
        Пулл реквесты
      </Typography>
      {pullRequests && pullRequests.length > 0 ?  <List>
          {pullRequests.map((todo, index) => (
              <PullRequest data={todo} key={index.toString()} dense button />
          ))}
      </List> : <Typography sx={{marginTop: '5px'}} gutterBottom variant="body2" component="div">Нет открытых пулл реквестов</Typography>}
    </CardContent>
  );
};

export default PullRequests;
