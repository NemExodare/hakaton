import React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import pr from "../../assets/pr.png";
import Link from "@mui/material/Link";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import Badge from "@mui/material/Badge";
import {styled} from "@mui/material/styles";
import {StyledBadge} from "../Header";
import {Chip} from "@material-ui/core";
import Stack from "@mui/material/Stack";

function PullRequest(props) {
  // const { todos, deleteTodo } = useTodoState([]);

    const {data} = props;

  return (
    <CardContent sx={{ textAlign: 'left' }}>
        <ListItem>
            <ListItemAvatar>
                {data.hasUpdates ? (
                        <StyledBadge
                            overlap="circular"
                            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                            variant="dot"
                        >
                            <Avatar src={pr}/>
                        </StyledBadge>
                    ) :
                    <Avatar src={pr}/>
                }
            </ListItemAvatar>
            {/*<Stack direction="row" spacing={2} sx={{padding: '5px'}} >*/}
            <Link href="#"><ListItemText primary={data.name} secondary={data.fromUser} /></Link>
            <Chip sx={{marginLeft: '10px'}} label={`Открыт ${data.openDays}д.`} color={`${data.openDays > 1 ? "error" : "default"}`} />
            {/*</Stack>*/}
        </ListItem>
        <Divider variant="inset" component="li" />
    </CardContent>
  );
};


export default PullRequest;
