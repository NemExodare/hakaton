import * as React from 'react';
import CardContent from '@mui/material/CardContent';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import photo1 from '../assets/aMt4IVJwtZs.png';
import photo2 from '../assets/KbRbrUFJ528.jpg';
import photo3 from '../assets/nbKrZl4Plkk.jpg';
import photo4 from '../assets/DpnIi-FKbIM.jpg';
import photo5 from '../assets/123.jpg';

function Team(props) {

    const {users} = props.team;
    const arrayPhoto = [
        {id: '3a25da55-20ab-4d3f-b777-2e1254b4dd5e', photo: photo1},
        {id: 'a3adb37e-aa2d-4e38-af35-d3bc2b7ce417', photo: photo4},
        {id: '767f6d9a-6156-433f-9d4c-c8b25ec195c0', photo: photo3},
        {id: '5c9d7d9a-abdb-48b1-a54f-592597cd71b6', photo: photo2},
        {id: 'abc06fef-fb52-4d60-8939-09ffe2466510', photo: photo5}
    ]

    return (
        <CardContent>
            <Typography gutterBottom variant="h6" component="div">
                Команда
            </Typography>
                {users && users.map((user, index) => (
                    <div style={user.id === props.userId ?
                        {border: '3px solid #50b3ff', borderRadius: '15px',  width: '75%', boxShadow: '5px 3px 7px #50b3ff'}
                        : {}} >
                        <Stack key={index} direction="row" spacing={2} sx={{padding: '5px'}}>
                            <Paper elevation={0} >
                                <Stack direction="row" spacing={2}>
                                    <Badge
                                        overlap="circular"
                                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                        // badgeContent={
                                        //     <SmallAvatar alt="Remy  Sharp" src='./static/media/nbKrZl4Plkk.f65762d7.jpg' />
                                        // }
                                    >
                                        <Avatar alt="Travis Howard" src={arrayPhoto.find(item => item.id === user.id) ? arrayPhoto.find(item => item.id === user.id).photo : ''} />
                                    </Badge>
                                </Stack>
                            </Paper>
                            <Paper elevation={0}>
                                <Typography variant="subtitle1" component="div" sx={{ marginTop: '10px' }}>
                                    <Link href="#" style={{ textDecoration: 'none', color: '#666666', fontWeight: 'bolder'}}>
                                        {`${user.lastname} ${user.firstname}`}
                                    </Link>
                                </Typography>
                            </Paper>
                            <Paper elevation={0}><Avatar alt="Travis Howard" src={props.emotions.find(emotion => emotion.id === user.emotion) ? props.emotions.find(emotion => emotion.id === user.emotion).img : ''} sx={{ width: 45, height: 45 }}/></Paper>
                        </Stack>
                    </div>
                ))}
        </CardContent>
    );
}
const BorderAvatar = styled(Stack)(({ theme }) => ({
    border: `2px solid ${theme.palette.background.paper}`,
}));

const SmallAvatar = styled(Avatar)(({ theme }) => ({
    width: 22,
    height: 22,
    border: `2px solid ${theme.palette.background.paper}`,
}));


export default Team;