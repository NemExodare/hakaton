import * as React from 'react';
import { useState, useEffect } from 'react';
import CardContent from '@mui/material/CardContent';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import axios from 'axios';
import { MenuItem } from "@mui/material";
import { Select } from "@material-ui/core";
import logo from "../assets/logo.jpg";

function Header(props) {

    const [selectOpened, setSelectOpened] = useState(false);
    const [currentEmotion, setCurrentEmotion] = useState(null);

    const handleEmotionChange = async (e) => {
        try {
            const value = e.target.value;
            setSelectOpened(false);
            const response = await axios.post(`http://localhost:8080//user/emotion?emotion=${value}&userId=${props.user.id}`);
            setCurrentEmotion(value)
            props.updateTeam();
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <CardContent sx={{ padding: '5px!important' }}>
            <Grid
                container
                direction="row"
                justifyContent="space-between"
                alignItems="center"
            >
                <Grid item sx={{display: 'flex', alignItems: 'center'}}>
                    <img src={logo} style={{height:'35px', marginRight: '5px'}}/>
                    <Typography gutterBottom variant="button" component="h5" sx={{marginTop: '3px'}}>
                        {`DashBoard разработчика. ${props.team.name && "Команда " + `"${props.team.name}"`}`}
                    </Typography>
                </Grid>
                <Grid item>
                    <Stack direction="row" spacing={2}>
                        <Paper elevation={0}>
                        </Paper>
                        <Paper elevation={0}><Typography variant="subtitle1" component="div" sx={{ marginTop: '5px' }}>{`${props.user.lastname} ${props.user.firstname}`}</Typography></Paper>
                        <Paper elevation={0}>
                            <Stack direction="row" spacing={2}>
                                <StyledBadge
                                    overlap="circular"
                                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
                                    variant="dot"
                                >

                                    <Avatar onClick={() => setSelectOpened(true)} alt="Remy Sharp" src={props.user.id && props.emotions.find(emotion => emotion.id === (currentEmotion ? currentEmotion : props.user.emotion)) ?
                                        props.user.id && props.emotions.find(emotion => emotion.id === (currentEmotion ? currentEmotion : props.user.emotion)).img
                                        : ''} />

                                    {selectOpened && <Select
                                        open={selectOpened}
                                        value={currentEmotion || props.user.emotion}
                                        onChange={handleEmotionChange}
                                    >
                                        {props.emotions.map((emotion, index) => (
                                            <MenuItem value={emotion.id}><Avatar alt="Remy Sharp" src={emotion.img} /></MenuItem>))
                                        }
                                    </Select>
                                    }

                                </StyledBadge>
                            </Stack>
                        </Paper>
                    </Stack>
                </Grid>
            </Grid>
        </CardContent>
    );
}

export const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: 'ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}));

export default Header;