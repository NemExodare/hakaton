import React from 'react';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import useTodoState from './useTodoState';

function Note() {
  const { todos, addTodo, deleteTodo } = useTodoState([]);

  return (
    <CardContent sx={{ textAlign: 'center' }}>

      <TodoForm
        saveTodo={todoText => {
          const trimmedText = todoText.trim();

          if (trimmedText.length > 0) {
            addTodo(trimmedText);
          }
        }}
      />
        <TodoList todos={todos || []} deleteTodo={deleteTodo} />
    </CardContent>
  );
};

export default Note;
